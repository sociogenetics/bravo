# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import CreateView
from django.utils.translation import ugettext as _
from django.contrib import messages

from xendor.cbv import PageAppExtensionMixin, WithMailer
from xendor.settings import XendorSettings

from .models import Capture


class Index(PageAppExtensionMixin, WithMailer, CreateView):
    """индекс"""

    model = Capture
    template_name = 'capture/index.html'
    app_extension = u'capture'

    def get_success_url(self):
        return reverse('capture-index') + u'#form'

    def form_valid(self, form):
        self.send_mail(
            u'Форма обратной связи на сайте {site_name}',
            'capture/mail.html',
            {'value': form.cleaned_data}, XendorSettings().get(u'E-mail для страницы захвата'))

        messages.add_message(self.request, messages.SUCCESS, _(u'Спасибо! Заявка отправлена администрации сайта.'))
        return super(Index, self).form_valid(form)