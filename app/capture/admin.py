# -*- coding: utf-8 -*-
from django.contrib import admin

from grappelli_modeltranslation.admin import TranslationAdmin

from .models import Capture


class CaptureAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    list_display = ('created', 'name', 'mail')


admin.site.register(Capture, CaptureAdmin)