# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='capture',
    app_name=u'Страница захвата',
    node_url=lambda: reverse('capture-index'),
    children=[],
    node_parameters={},
    safe_for_structure=True
)



