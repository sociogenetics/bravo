# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from .views import Index

urlpatterns = patterns('',
    url(r'^$', Index.as_view(), name = 'capture-index'),
)