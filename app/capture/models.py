# -*- coding: utf-8 -*-
from django.db import models

from app.catalog.models import Type

         
class Capture(models.Model):
    """Сообщения со страницы захвата"""

    name = models.CharField(u'Имя', max_length=245)
    mail = models.CharField(u'E-mail', max_length=255)
    phone = models.CharField(u'Телефон', max_length=255)
    type = models.ForeignKey(Type, verbose_name=u'Тип')

    created = models.DateField(u'Дата публикации', auto_now_add=True)

    def __unicode__(self):
        return unicode(self.created)

    class Meta:
        ordering = 'created',
        verbose_name = u'Заявка'
        verbose_name_plural = u'Заявки'