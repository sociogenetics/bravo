# -*- coding: utf-8 -*-
import datetime

from django.db import models
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

from tinymce.models import HTMLField
from ajaximage.fields import AjaxImageField

from xendor.models import Page
from xendor.utils import generate_slug
from xendor.utils import getmd5


#todo: похерить вместе с миграциями
#бутор для совместимости с джанговскими миграциями
def upload_city(instance, filename):
    pass


def upload_item(instance, filename):
    pass


def upload_city_image(instance, filename):
    pass


def upload_region_image(instance, filename):
    pass


def upload_item_image(instance, filename):
    pass


def upload_page_image(instance, filename):
    pass


class City(models.Model):
    """ Города Турции """

    name = models.CharField(u'Название', max_length=245)
    description = HTMLField(u'Описание', blank=True, null=True)
    image = AjaxImageField(u'Изображение', upload_to='catalog/city', blank=True, null=True)
    order = models.IntegerField(u'Порядок вывода', default=350)
    visible = models.BooleanField(u'Показывать', default = True)
    slug = models.SlugField(u'Слаг', max_length=255, null=True, unique=True, blank=True)

    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    @models.permalink
    def get_absolute_url(self):
        return ('catalog-city', [self.slug])

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.name)
        super(City, self).save(*args, **kwargs)

    def get_regions(self):
        return self.region_set.filter(visible=True)

    @classmethod
    def get_all_regions(cls):
        cities = cls.objects.filter(visible=True)
        result = [('', _(u'-- Выберите регион'))]
        for city in cities:
            result.append(
                (city.name, [(r.pk, r.name) for r in city.get_regions()])
            )
        return result

    class Meta:
        ordering = 'order', 'name'
        verbose_name = u'город'
        verbose_name_plural = u'Города'


class CityImage(models.Model):
    """Изображение в городе"""

    city = models.ForeignKey(City, verbose_name=u'город', null=True)
    image = AjaxImageField(u'Изображение', upload_to='catalog/cityimage', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return unicode(self.city) + ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'


class Region(models.Model):
    """ Регион """

    name = models.CharField(u'Название', max_length=245)
    city = models.ForeignKey(City, verbose_name=u'Город')
    description = HTMLField(u'Описание', blank=True, null=True)
    order = models.IntegerField(u'Порядок вывода', default=350)
    slug = models.SlugField(u'Слаг', max_length=255, null=True, unique=True, blank=True)
    visible = models.BooleanField(u'Показывать', default = True)

    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    @models.permalink
    def get_absolute_url(self):
        return ('catalog-objects-by-region', [self.slug])

    def __unicode__(self):
        return self.city.name + u'/' + self.name

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.name)
        super(Region, self).save(*args, **kwargs)

    class Meta:
        ordering = 'order', 'city__name', 'name'
        verbose_name = u'Регион'
        verbose_name_plural = u'Регионы'


class RegionImage(models.Model):
    """ Изображение в городе """

    region = models.ForeignKey(Region, verbose_name=u'Район', null=True)
    image = AjaxImageField(u'Изображение', upload_to='catalog/region', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return unicode(self.region) + ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'


class Type(models.Model):
    """ Типы недвижимости """
    
    name = models.CharField(u'Название', max_length=245)
    description = HTMLField(u'Описание', blank=True, null=True)
    order = models.IntegerField(u'Порядок вывода', default=350)
    visible = models.BooleanField(u'Показывать', default = True)
    slug = models.SlugField(u'Слаг', max_length=255, null=True, unique=True, blank=True)

    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    def __unicode__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.name)
        super(Type, self).save(*args, **kwargs)
        
    def is_sale_active(self):
        return Item.objects.filter(type=self, visible=True).exclude(deal='rent').count() > 0

    def is_rent_active(self):
        return Item.objects.filter(type=self, visible=True).exclude(deal='sale').count() > 0
        
    class Meta:
        ordering = 'order', 'name'
        verbose_name = u'тип'
        verbose_name_plural = u'Типы недвижимости'


class Item(models.Model):
    """ Объект недвижимости """
    
    DEAL_CHOICES = (  # Тип сделки
        (u'both', u'Продажа и аренда'),
        (u'sale', _(u'Продажа')),
        (u'rent', _(u'Аренда')),
    )
    
    RENT_TIME_CHOICES = (
        (u'month', _(u'месяц')),
        (u'week', _(u'неделю')),
        (u'day', _(u'сутки')),
    )

    SLEEPING_ROOM = (
        (u'studio', _(u'Студия')),
        (u'1', _(u'1')),
        (u'2', _(u'2')),
        (u'more2', _(u'Более 2')),
    )

    updated = models.DateTimeField(u'Обновленно', auto_now=True)
    codename = models.CharField(u'Код объекта', max_length=255, unique=True)
    
    deal = models.CharField(u'Тип сделки', choices=DEAL_CHOICES, default=u'sale', max_length=255)

    region = models.ForeignKey(Region, verbose_name=u'Регион', null=True)
    type = models.ForeignKey(Type, verbose_name=u'Тип объекта', null=True)
    title = models.CharField(u'Заголовок объекта', max_length=255)
    
    lat = models.CharField(u'Широта', max_length=255, blank=True, null=True)
    lng = models.CharField(u'Долгота', max_length=255, blank=True, null=True)
    
    short_description = models.TextField(u'Краткое описание', blank=True, null=True)
    description = HTMLField(u'Полное описание', blank=True, null=True)

    square_live = models.DecimalField(u'Жилая площадь', max_digits=10, decimal_places=2, help_text=u'Квадратных метров', default=0)
    square_all = models.DecimalField(u'Общая площадь', max_digits=10, decimal_places=2, help_text=u'Квадратных метров', default=0)
    square = models.DecimalField(u'Площадь земли', max_digits=10, decimal_places=2, help_text=u'Квадратных метров', default=0)

    sleeping_room = models.CharField(u'Количество спален', max_length=255, default=u'1', choices=SLEEPING_ROOM)
    bath_room = models.IntegerField(u'Количество ванн', default=0)
    
    price_sale = models.DecimalField(u'Цена продажи', max_digits=10, decimal_places=2, help_text=u'Цена задается в евро', default=0)
    price_sale_discount = models.DecimalField(u'Цена продажи со скидкой', max_digits=10, decimal_places=2, help_text=u'Цена задается в евро', default=0)
    price_rent = models.DecimalField(u'Цена аренды', max_digits=10, decimal_places=2, help_text=u'Цена задается в евро', default=0)
    price_rent_discount = models.DecimalField(u'Цена аренды со скидкой', max_digits=10, decimal_places=2, help_text=u'Цена задается в евро', default=0)
    
    rent_time = models.CharField(u'Цена аренды за', max_length=255, choices=RENT_TIME_CHOICES, default = u'month')
    
    image = AjaxImageField(u'Основное изображение', upload_to='catalog/item', blank=True, null=True)
    
    visible = models.BooleanField(u'Показывать', default=True)
    slug = models.SlugField(u'Слаг', max_length=255, null=True, unique=True, blank=True)

    is_new = models.BooleanField(u'Новинка', default=False)
    is_special = models.BooleanField(u'Спецпредложение', default=False)
    is_sold = models.BooleanField(u'Продано', default=False)

    view_count = models.IntegerField(u'Количество просмотров', default=0)

    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    #данные владельца
    person = models.CharField(u'Персона', max_length=255, blank=True, null=True)
    mail = models.CharField(u'E-mail', max_length=255, blank=True, null=True)
    phone = models.CharField(u'Телефон', max_length=255, blank=True, null=True)

    @models.permalink
    def get_absolute_url(self):
        return ('catalog-item', [self.slug])
    
    def __unicode__(self):
        return self.title

    def get_price(self):
        if self.deal in (u'sale', u'both'):
            return self.price_sale
        else:
            return self.price_rent

    def get_sea_distance(self):
        try:
            value = ObjectProperty.objects.get(item=self, tag=32)
            print value
            return value.value
        except ObjectProperty.DoesNotExist:
            return None

    def get_sleeping_room(self):
        return [i[1] for i in self.SLEEPING_ROOM if i[0] == self.sleeping_room][0]

    def get_property(self):
        properties = []
        categories = TagCategory.objects.filter(
            pk__in=self.objectproperty_set.order_by('tag__category').values_list('tag__category').distinct())

        for category in categories:
            properties.append(
                (
                    category.name,
                    ObjectProperty.objects.filter(
                        tag__in=category.tag_set.all(),
                        item=self
                    )
                )
            )
        return properties

    def get_tags(self):
        properties = []
        categories = TagCategory.objects.all()

        for category in categories:
            tags = category.tag_set.all()
            for tag in tags:
                try:
                    tag.object_value = ObjectProperty.objects.get(tag=tag, item=self).get_value()
                except:
                    tag.object_value = u'--'
            properties.append(
                (
                    category.name,
                    tags
                )
            )
        return properties
    
    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.title)
        super(Item, self).save(*args, **kwargs)
        
    class Meta:
        ordering = '-updated',
        verbose_name = u'Объект'
        verbose_name_plural = u'Объекты'


class ItemImage(models.Model):
    """ Изображение объекта недвижимости """

    item = models.ForeignKey(Item, verbose_name=u'Объект')
    image = AjaxImageField(u'Изображение', upload_to='catalog/itemimage', blank=True)
    # image = models.ImageField(u'Изображение', upload_to='catalog/itemimage', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return unicode(self.item) + ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'


class PageImage(models.Model):
    """ Изображение в странице """

    page = models.ForeignKey(Page, verbose_name=u'Страницв', null=True)
    image = AjaxImageField(u'Изображение', upload_to='pages', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return unicode(self.page) + ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'


class TagCategory(models.Model):
    """ Категория тегов """
    
    name = models.CharField(u'Название', max_length=255)
    order = models.IntegerField(u'Порядок вывода', default=350)
   
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = 'order', 'name'
        verbose_name = u'категория тегов'
        verbose_name_plural = u'Категории тегов'
        

class Tag(models.Model):
    """ Тег недвижимости """

    category = models.ForeignKey(TagCategory, verbose_name = u'Категория тега')    
    name = models.CharField(u'Название', max_length = 255)
   
    def __unicode__(self):
        return unicode(self.category) + u' / ' + self.name
    
    class Meta:
        ordering = 'category', 'name'
        verbose_name = u'тег'
        verbose_name_plural = u'Теги'


class TagValue(models.Model):
    """ Типовое значение тега """
    
    name = models.CharField(u'Название', max_length = 255)

    def __unicode__(self):
        return self.name
     
    class Meta:
        ordering = 'name',
        verbose_name = u'Типовое значение тега'
        verbose_name_plural = u'Типовые значения тега'


class ObjectProperty(models.Model):
    """ Свойства объекта """

    item = models.ForeignKey(Item, verbose_name=u'Объект')
    tag = models.ForeignKey(Tag, verbose_name=u'Свойство объекта', blank = False)
    value = models.CharField(u'Значение', max_length=255,  null=True, blank=True)
    tagvalue = models.ForeignKey(TagValue, verbose_name=u'Типовое значение объекта', null=True, blank=True)
    
    def __unicode__(self):
        return unicode(self.tag)  # + u' : ' + self.value

    def get_value(self):
        if self.value:
            return self.value
        else:
            if self.tagvalue:
                return self.tagvalue
        
    class Meta:
        ordering = 'tag__category__name',
        verbose_name = u'свойство объекта'
        verbose_name_plural = u'Свойства объекта'

            
class Order(models.Model):
    """ Заявки """

    item = models.ForeignKey(Item, verbose_name=u'Объект')
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    person = models.CharField(_(u'Ваше имя'), max_length=255)
    phone = models.CharField(_(u'Телефон'), max_length=255)
    mail = models.EmailField(_(u'E-mail'), max_length=255)

    comment = models.TextField(_(u'Комментарий'), blank=True, null=True)

    def __unicode__(self):
        return unicode(self.item) + u'::' + unicode(self.created)

    class Meta:
        verbose_name = u'Заявка'
        verbose_name_plural = u'Заявки'
