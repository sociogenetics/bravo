# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from .models import City, CityImage, Region, RegionImage, Type, Item, ItemImage, TagCategory, Tag, TagValue, \
    ObjectProperty, PageImage

from xendor.models import Page, Fragment


class CityTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'meta_title', 'meta_description', 'meta_keywords')


class CityImageTranslationOptions(TranslationOptions):
    fields = ('title', )


class RegionTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'meta_title', 'meta_description', 'meta_keywords')


class RegionImageTranslationOptions(TranslationOptions):
    fields = ('title', )


class TypeTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'meta_title', 'meta_description', 'meta_keywords')


class ItemTranslationOptions(TranslationOptions):
    fields = ('title', 'short_description', 'description', 'meta_title', 'meta_description', 'meta_keywords')


class ItemImageTranslationOptions(TranslationOptions):
    fields = ('title', )


class TagCategoryTranslationOptions(TranslationOptions):
    fields = ('name', )


class TagTranslationOptions(TranslationOptions):
    fields = ('name', )


class TagValueTranslationOptions(TranslationOptions):
    fields = ('name', )


class ObjectPropertyTranslationOptions(TranslationOptions):
    fields = ('value', )


class PageTranslationOptions(TranslationOptions):
    fields = ('title', 'menu_title', 'content', 'meta_title', 'meta_description', 'meta_keywords',)


class FragmentTranslationOptions(TranslationOptions):
    fields = ('content',)


class PageImageTranslationOptions(TranslationOptions):
    fields = ('title', )


translator.register(City, CityTranslationOptions)
translator.register(CityImage, CityImageTranslationOptions)
translator.register(Region, RegionTranslationOptions)
translator.register(RegionImage, RegionImageTranslationOptions)
translator.register(Type, TypeTranslationOptions)
translator.register(Item, ItemTranslationOptions)
translator.register(ItemImage, ItemImageTranslationOptions)
translator.register(TagCategory, TagCategoryTranslationOptions)
translator.register(Tag, TagTranslationOptions)
translator.register(TagValue, TagValueTranslationOptions)
translator.register(ObjectProperty, ObjectPropertyTranslationOptions)
translator.register(Page, PageTranslationOptions)
translator.register(PageImage, PageImageTranslationOptions)
translator.register(Fragment, FragmentTranslationOptions)