# -*- coding: utf-8 -*-
from django import template
from django.db.models import Count
from django.conf import settings

from app.catalog.models import Item, Type, City, Region

register = template.Library()


@register.assignment_tag
def catalog_regions():
    regions = []
    # for item in Area.objects.filter(visible=True):
    #     if item.region.pk in regions: continue
    #
    #     regions.append(item.region.pk)

    return Region.objects.filter(pk__in=regions)


@register.assignment_tag(takes_context=True)
def lang_selector(context, lang):

    result = ''
    try:
        lang_set = [l[0] for l in settings.LANGUAGES]
        request = context['request']
        path = request.get_full_path().split('/')
        if path[1] in lang_set:
            result = '/' + lang + '/' + '/'.join(path[2:])
    except:
        pass

    return result


@register.assignment_tag
def catalog_cities():
    return City.objects.filter(visible=True)


@register.assignment_tag
def catalog_cities_5():
    return City.objects.filter(visible=True).order_by('?')[:5]


@register.assignment_tag
def catalog_last_sale():
    return Item.objects.filter(visible=True).exclude(deal='rent').order_by('-updated')[:3]


@register.assignment_tag
def catalog_last_rent():
    return Item.objects.filter(visible=True).exclude(deal='sale').order_by('-updated')[:3]


@register.assignment_tag
def catalog_spec():
    return Item.objects.filter(visible=True, is_special=True).order_by('-updated')[:3]

@register.assignment_tag
def catalog_spec_sale():
    return Item.objects.filter(visible=True, is_special=True).exclude(deal='rent').order_by('-updated')[:3]


@register.assignment_tag
def catalog_spec_rent():
    return Item.objects.filter(visible=True, is_special=True).exclude(deal='sale').order_by('-updated')[:3]


@register.assignment_tag
def catalog_items():
    return Item.objects.filter(visible=True)


@register.assignment_tag
def catalog_types():
    return Type.objects.filter(visible=True).annotate(num_objects=Count('item')).filter(num_objects__gt=0)


@register.assignment_tag
def catalog_sleeping_rooms():
    return [(x[0], [y[1] for y in Item.SLEEPING_ROOM if y[0]==x[0]][0]) for x in Item.objects.filter(visible=True).order_by('sleeping_room').values_list('sleeping_room').distinct()]
    # return [x[0] for x in
    #         Item.objects.filter(visible=True).order_by('sleeping_room').values_list('sleeping_room').distinct()]


@register.inclusion_tag('catalog/tags/filtr.html', takes_context=True)
def catalog_filtr(context, deal):
    return {
        'deal': deal,
        'regions': Region.objects.all(),
        'types': Type.objects.filter(visible=True),
        'sleeping_rooms': [x[0] for x in Item.objects.filter(visible=True).order_by('sleeping_room').values_list(
        'sleeping_room').distinct()],
        'asf': '',
    }


@register.inclusion_tag('catalog/tags/promo.html', takes_context=True)
def catalog_promo(context):
    show = False
    if context['request'].path in ('/ru/', '/en/'):
        show = True

    context.update({
        'show_promo': show
    })
    return context