# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db.models import Count
# from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _


from xendor.structure import Structure

from app.catalog.models import Type, City, Region

Structure().register_app(
    app_id='catalog',
    app_name=u'Каталог объектов',
    node_url=lambda: reverse('catalog-index'),
    children=[
        {
            'title': _(u'Аренда недвижимости'),
            'url': lambda: reverse('catalog-rent'),
            'children': [
                (lambda t: {
                    'title': t.name,
                    'url': lambda: reverse('catalog-rent-by-type', args=[t.slug]),
                    'children': [],
                    'in_menu': True,
                    'parameters': {'count': t.num_objects, 'menu_type': 'type', 'objects': 'rent'},
                    'meta_title': t.meta_title or t.name,
                    'meta_description': t.meta_description or t.name,
                    'meta_keywords': t.meta_keywords or t.name,
                })(x)
                for x in Type.objects.filter(visible=True,
                                             item__deal__in=(u'rent', u'both')).annotate(num_objects=Count('item'))
            ] + [
                (lambda t: {
                    'title': t.name,
                    'url': lambda: reverse('catalog-rent-by-city', args=[t.slug]),
                    'children': [
                        (lambda d: {
                            'title': d.name,
                            'url': lambda: reverse('catalog-rent-by-region', args=[d.slug]),
                            'children': [],
                            'in_menu': True,
                            'parameters': {'count': d.num_objects},
                            'meta_title': d.meta_title or d.name,
                            'meta_description': d.meta_description or d.name,
                            'meta_keywords': d.meta_keywords or d.name,
                        })(y)
                        for y in Region.objects.filter(visible=True, city=t,
                                                       item__deal__in=(u'rent', u'both')).annotate(num_objects=Count('item'))
                    ],
                    'in_menu': True,
                    'parameters': {'count': t.num_objects, 'menu_type': 'city', 'objects': 'rent'},
                    'meta_title': t.meta_title or t.name,
                    'meta_description': t.meta_description or t.name,
                    'meta_keywords': t.meta_keywords or t.name,
                })(x)
                for x in City.objects.filter(region__item__deal__in=(u'rent', u'both')).
                            annotate(num_objects=Count('region__item')).filter(visible=True, num_objects__gt=0)
            ],
            'in_menu': True,
            'parameters': {},
            'meta_title': _(u'Аренда недвижимости'),
            'meta_description': _(u'Аренда недвижимости'),
            'meta_keywords': _(u'Аренда недвижимости')
        }, {
            'title': _(u'Продажа недвижимости'),
            'url': lambda: reverse('catalog-sale'),
            'children': [
                (lambda t: {
                    'title': t.name,
                    'url': lambda: reverse('catalog-sale-by-type', args=[t.slug]),
                    'children': [],
                    'in_menu': True,
                    'parameters': {'count': t.num_objects, 'menu_type': 'type'},
                    'meta_title': t.meta_title or t.name,
                    'meta_description': t.meta_description or t.name,
                    'meta_keywords': t.meta_keywords or t.name,
                })(x)
                for x in Type.objects.filter(visible=True,
                                             item__deal__in=(u'sale', u'both')).annotate(num_objects=Count('item'))
            ] + [
                (lambda t: {
                    'title': t.name,
                    'url': lambda: reverse('catalog-sale-by-city', args=[t.slug]),
                    'children': [
                        (lambda d: {
                            'title': d.name,
                            'url': lambda: reverse('catalog-sale-by-region', args=[d.slug]),
                            'children': [],
                            'in_menu': True,
                            'parameters': {'count': d.num_objects},
                            'meta_title': d.meta_title or d.name,
                            'meta_description': d.meta_description or d.name,
                            'meta_keywords': d.meta_keywords or d.name,
                        })(y)
                        for y in Region.objects.filter(visible=True, city=t,
                                             item__deal__in=(u'sale', u'both')).annotate(num_objects=Count('item'))
                    ],
                    'in_menu': True,
                    'parameters': {'count': t.num_objects, 'menu_type': 'city'},
                    'meta_title': t.meta_title or t.name,
                    'meta_description': t.meta_description or t.name,
                    'meta_keywords': t.meta_keywords or t.name,
                })(x)
                for x in City.objects.filter(region__item__deal__in=(u'sale', u'both')).
                            annotate(num_objects=Count('region__item')).filter(visible=True, num_objects__gt=0)
            ],
            'in_menu': True,
            'parameters': {},
            'meta_title': _(u'Продажа недвижимости'),
            'meta_description': _(u'Продажа недвижимости'),
            'meta_keywords': _(u'Продажа недвижимости')
        },
    ] + [
        (lambda t: {
            'title': t.name,
            'url': lambda: reverse('catalog-objects-by-type', args=[t.slug]),
            'children': [],
            'in_menu': True,
            'parameters': {'count': t.num_objects, 'menu_type': 'type', 'objects': 'all'},
            'meta_title': t.meta_title or t.name,
            'meta_description': t.meta_description or t.name,
            'meta_keywords': t.meta_keywords or t.name,
        })(x)
        for x in Type.objects.filter(visible=True).annotate(num_objects=Count('item'))
    ] + [
        (lambda t: {
            'title': t.name,
            'url': lambda: reverse('catalog-objects-by-city', args=[t.slug]),
            'children': [
                (lambda d: {
                    'title': d.name,
                    'url': lambda: reverse('catalog-objects-by-region', args=[d.slug]),
                    'children': [],
                    'in_menu': True,
                    'parameters': {'count': d.num_objects},
                    'meta_title': d.meta_title or d.name,
                    'meta_description': d.meta_description or d.name,
                    'meta_keywords': d.meta_keywords or d.name,
                })(y)
                for y in Region.objects.filter(visible=True, city=t).annotate(num_objects=Count('item'))
            ],
            'in_menu': True,
            'parameters': {'count': t.num_objects, 'menu_type': 'city', 'objects': 'all'},
            'meta_title': t.meta_title or t.name,
            'meta_description': t.meta_description or t.name,
            'meta_keywords': t.meta_keywords or t.name,
        })(x)
        for x in City.objects.annotate(num_objects=Count('region__item')).filter(visible=True, num_objects__gt=0)
    ],
    node_parameters={},
    safe_for_structure=False
)


Structure().register_app(
    app_id='post-object',
    app_name=u'Форма добавления объекта',
    node_url=lambda: reverse('catalog-post-object'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)


