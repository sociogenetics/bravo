# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt

from .views import ItemView, RentView, SaleView, OrderView, PrintItemView, ObjectsByRegionView, \
    SaleByTypeView, RentByTypeView, SaleByRegionView, RentByRegionView, \
    SaleByCityView, RentByCityView, ObjectsByTypeView, ObjectsByCityView, \
    SearchView, RegionView, CityView, SpecialRentView, SpecialSaleView, IndexView, \
    PostObjectView, ProcessImage4PostObject, PostObjectImagesView, XMLExportView

urlpatterns = patterns('',
    #Главная
    url(r'^$', IndexView.as_view(), name='catalog-index'),

    # блок добавления объектов
    url(r'^post-object/$', PostObjectView.as_view(), name='catalog-post-object'),
    url(r'^post-object-images/$', PostObjectImagesView.as_view(), name='catalog-post-object-images'),
    url(r'^process-image-post-object/$', csrf_exempt(ProcessImage4PostObject.as_view()), name='catalog-post-object-process-image'),

    #поиск
    url(r'^search/$', SearchView.as_view(), name='catalog-search'),

    #заказ
    url(r'^order/$', OrderView.as_view(), name='catalog-order'),
    url(r'^order-success/$', TemplateView.as_view(template_name='catalog/order/order-success.html'),
        name='catalog-order-success'),

    #Города и районы
    url(r'^city/(?P<slug>[\w-]+)/$', CityView.as_view(), name='catalog-city'),
    url(r'^region/(?P<slug>[\w-]+)/$', RegionView.as_view(), name='catalog-region'),

    #######Продажа######
    #Продажа
    url(r'^sale/$', SaleView.as_view(), name='catalog-sale'),
    url(r'^special-sale/$', SpecialSaleView.as_view(), name='catalog-special-sale'),

    #Продажа по типам недвижки
    url(r'^sale-by-type/(?P<slug>[\w-]+)/$', SaleByTypeView.as_view(), name='catalog-sale-by-type'),

    #Продажа по городам
    url(r'^sale-by-region/(?P<slug>[\w-]+)/$', SaleByRegionView.as_view(), name='catalog-sale-by-region'),

    #Продажа по муниципалитетам
    url(r'^sale-by-city/(?P<slug>[\w-]+)/$', SaleByCityView.as_view(), name='catalog-sale-by-city'),


    ######все объекты######
    # #объекты по регионам
    url(r'^objects-by-region/(?P<slug>[\w-]+)/$', ObjectsByRegionView.as_view(), name='catalog-objects-by-region'),

    #объекты по городам
    url(r'^objects-by-city/(?P<slug>[\w-]+)/$', ObjectsByCityView.as_view(), name='catalog-objects-by-city'),

    #объекты по типам недвижки
    url(r'^objects-by-type/(?P<slug>[\w-]+)/$', ObjectsByTypeView.as_view(), name='catalog-objects-by-type'),


    #######Аренда#######
    #Аренда
    url(r'^rent/$', RentView.as_view(), name='catalog-rent'),
    url(r'^special-rent/$', SpecialRentView.as_view(), name='catalog-special-rent'),

    #Аренда по типам недвижки
    url(r'^rent-by-type/(?P<slug>[\w-]+)/$', RentByTypeView.as_view(), name='catalog-rent-by-type'),

    #Аренда по региону
    url(r'^rent-by-region/(?P<slug>[\w-]+)/$', RentByRegionView.as_view(), name='catalog-rent-by-region'),

    #Аренда по муниципалитетам
    url(r'^rent-by-city/(?P<slug>[\w-]+)/$', RentByCityView.as_view(), name='catalog-rent-by-city'),


    #######Прочее######
    #объект на печать
    url(r'^print/(?P<slug>[\w-]+)/$', PrintItemView.as_view(), name='catalog-item-print'),
    #страница объекта
    url(r'^(?P<slug>[\w-]+)/$', ItemView.as_view(), name='catalog-item'),

    #Экспорт в XML
    url(r'^xml-export\.xml$', XMLExportView.as_view(), name='catalog-xml'),
)