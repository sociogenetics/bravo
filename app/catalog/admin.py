# -*- coding: utf-8 -*-
from django.contrib import admin
from xendor.admin_utils import image_field
from grappelli_modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from app.catalog.models import Type, City, TagCategory, Tag, Order, Item, Region, ItemImage, ObjectProperty, \
    TagValue, CityImage, RegionImage

#
# class RegionImageInline(TranslationTabularInline):
#     model = RegionImage
#     extra = 0


class RegionAdmin(TranslationAdmin):
    list_display = 'name', 'order',
    list_display_links = 'name',
    list_editable = 'order',
    list_filter = 'city',
    
    # inlines = [RegionImageInline, ]

    
admin.site.register(Region, RegionAdmin)


class CityImageInline(TranslationTabularInline):
    model = CityImage
    extra = 0


class RegionInline(TranslationTabularInline):
    model = Region
    extra = 0
    fields = 'name', 'order',


class CityAdmin(TranslationAdmin):
    list_display = 'poster', 'name', 'order', 'visible'
    list_display_links = 'name',
    list_editable = 'order', 'visible'
    poster = image_field('image', u'Изображение', u'50;50;fix')
    
    inlines = [CityImageInline, RegionInline]


admin.site.register(City, CityAdmin)


class TypeAdmin(TranslationAdmin):
    list_display = 'name', 'order', 'visible'
    list_display_links = 'name',
    list_editable = 'order', 'visible'

admin.site.register(Type, TypeAdmin)


class TagInline(TranslationTabularInline):
    model = Tag
    extra = 0


class TagCategoryAdmin(TranslationAdmin):
    list_display = 'name', 'order'
    list_display_links = 'name',
    list_editable = 'order',
    
    inlines = [TagInline, ]


admin.site.register(TagCategory, TagCategoryAdmin)


admin.site.register(Order)


class ImageInline(TranslationTabularInline):
    model = ItemImage
    extra = 0


class ObjectPropertyInline(TranslationTabularInline):
    model = ObjectProperty
    extra = 0


class ItemAdmin(TranslationAdmin):
    list_display = 'poster', 'codename', 'deal', 'title', 'region', 'view_count', 'is_new', 'is_special', 'is_sold'
    list_display_links = 'poster', 'codename',
    
    list_editable = 'is_special', 'is_new', 'is_sold'
    
    readonly_fields = 'view_count', 'updated'
    
    list_filter = 'deal', 'type', 'sleeping_room', 'bath_room', 'is_special'
    
    poster = image_field('image', u'Изображение', u'50;50;fix')
    
    inlines = [ObjectPropertyInline, ImageInline,]
    
    fieldsets = (
        ('', {
            'classes': ('opened',),
            'fields': (
                ('deal', 'codename',),
                'title',
                ('region', 'type', ),
                ('lat', 'lng'),
                'image',
                'short_description',
                ('description',),
                ('square_live', 'square_all', 'square'),
                ('price_sale', 'price_rent', 'rent_time'),
                ('price_sale_discount', 'price_rent_discount', 'updated'),
                ('sleeping_room', 'bath_room', 'view_count'),
                ('visible', 'is_new', 'is_special', 'is_sold'),
                'slug',
            ),
        }),
        ('Метатеги', {
            'classes': ('grp-open', ),
            'fields': (
                'meta_title',
                'meta_description',
                'meta_keywords',
            ),
        }),
        ('Данные владельца', {
            'classes': ('grp-close', ),
            'fields': (
                'person',
                'mail',
                'phone',
            ),
        }),
    )


admin.site.register(Item, ItemAdmin)


admin.site.register(TagValue, TranslationAdmin)
admin.site.register(Tag, TranslationAdmin)


# На будущее xendor переопределяется в основном апликейшене проекта
from xendor.models import Page, Fragment
from xendor.forms import PageAdminForm
from xendor.tree_admin import XendorTreeModelAdmin

from app.catalog.models import PageImage

admin.site.unregister(Page)


class PageImageInline(TranslationTabularInline):
    model = PageImage
    extra = 0


class PageAdmin(XendorTreeModelAdmin, TranslationAdmin):
    fieldsets = (
        ('', {
            'classes': ('closed',),
            'fields': ('title', 'menu_title', 'content', 'in_menu'),
        }),
        ('Метаданные', {
            'classes': ('grp-collapse grp-closed',),
            'description': 'Используются поисковиками для лучшей индексации страницы',
            'fields': ('meta_title', 'meta_description', 'meta_keywords'),
        }),
        ('Настройки', {
            'classes': ('grp-collapse grp-closed',),
            'description': 'Без четкой уверенности сюда лучше не лезть',
            'fields': ('slug', 'visible', 'parameters', 'template', 'app_extension', 'menu_url', 'is_main'),  # 'template',
        }),
        ('В структуре сайта..', {
            'classes': ('hidden',),
            'fields': ('parent',),
        })
    )

    list_display = ['actions_column', 'indented_short_title', 'app_extension']
    list_filter = ('visible', )

    def drag(self, obj):
        return '<div class="drag_handle"></div>'

    drag.allow_tags = True

    form = PageAdminForm

    inlines = [PageImageInline,]

admin.site.register(Page, PageAdmin)

admin.site.unregister(Fragment)

admin.site.register(Fragment, TranslationAdmin)