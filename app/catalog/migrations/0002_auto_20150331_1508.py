# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='mail',
            field=models.CharField(max_length=255, null=True, verbose_name='E-mail', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='person',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041f\u0435\u0440\u0441\u043e\u043d\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='phone',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
    ]
