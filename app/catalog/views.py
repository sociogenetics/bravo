# -*- coding: utf-8 -*-
import time

from django.contrib import messages
from django.core.files import File
from django.core.urlresolvers import reverse
from django import forms
from django.db.models import Count, Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, TemplateView, CreateView, FormView
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from captcha.fields import CaptchaField

from modeltranslation.utils import auto_populate

from xendor.cbv import ToStructureMixin, PaginatedListMixin, PageAppExtensionMixin, VisibleObjectListMixin, \
    WithMailer, ListByObjectSlugMixin, SearchByModelMixin

from xendor.settings import XendorSettings

from .models import Order, Item, Type, City, Region, ObjectProperty, Tag, TagValue, TagCategory, ItemImage


class IsRentMixin(object):
    """Тупо добавляет флаг rent в контекст"""

    def get_context_data(self, **kwargs):
        context = super(IsRentMixin, self).get_context_data(**kwargs)
        context.update({'rent': True})
        return context


class IndexView(ToStructureMixin, PageAppExtensionMixin, VisibleObjectListMixin, PaginatedListMixin):
    """Корневая страница недвижки"""

    app_extension = 'catalog'
    template_name = 'catalog/index.html'
    model = Item
    paginate_by = 12


class RentView(ToStructureMixin, IsRentMixin, VisibleObjectListMixin, PaginatedListMixin):
    """все объекты аренды"""

    template_name = 'catalog/rent.html'
    model = Item
    paginate_by = 12

    def get_queryset(self):
        return super(RentView, self).get_queryset().filter(deal__in=(u'rent', u'both'))


class SaleView(ToStructureMixin, VisibleObjectListMixin, PaginatedListMixin):
    """все объекты продажи"""

    template_name = 'catalog/sale.html'
    model = Item
    paginate_by = 12

    def get_queryset(self):
        return super(SaleView, self).get_queryset().filter(deal__in=(u'sale', u'both'))


class SpecialRentView(ToStructureMixin, IsRentMixin, VisibleObjectListMixin, PaginatedListMixin):
    """Специальные предложения аренды"""

    template_name = 'catalog/special-rent.html'
    model = Item
    paginate_by = 12
    activated_node = lambda o: reverse('catalog-rent')
    meta_title = meta_description = breadcrumbs = u'Спецпредложения аренды недвижимости'

    def get_queryset(self):
        return super(SpecialRentView, self).get_queryset().filter(deal__in=(u'rent', u'both'),
                                                                  is_special=True)


class SpecialSaleView(ToStructureMixin, VisibleObjectListMixin, PaginatedListMixin):
    """продажа"""

    template_name = 'catalog/special-sale.html'
    model = Item
    paginate_by = 12
    activated_node = lambda o: reverse('catalog-sale')
    meta_title = meta_description = breadcrumbs = u'Спецпредложения продажи недвижимости'

    def get_queryset(self):
        return super(SpecialSaleView, self).get_queryset().filter(deal__in=(u'sale', u'both'),
                                                                  is_special=True)


class OrderForm(forms.ModelForm):
    captcha = CaptchaField(label=_(u'Проверочный код '))

    class Meta:
        model = Order
        widgets = {
            'item': forms.HiddenInput()
        }


class ItemView(ToStructureMixin, DetailView):
    """объект"""

    model = Item
    activated_node = lambda o: reverse('catalog-index')
    template_name = 'catalog/item.html'
    meta_title = meta_description = lambda o: o.get_object().title
    breadcrumbs = lambda o: o.get_object().title

    def get_context_data(self, **kwargs):
        context = super(ItemView, self).get_context_data(**kwargs)

        views = self.request.session.get('views', [])

        if not unicode(self.object.id) in views:
            views.append(unicode(self.object.id))
            self.object.view_count += 1
            self.object.save()
            self.request.session['views'] = views

        order = Order(item=self.get_object())
        context.update({
            'form': OrderForm(instance=order),
            'rent': self.object.deal != 'sale'
        })

        return context


class PrintItemView(ToStructureMixin, DetailView):
    """Печать объекта"""

    model = Item
    activated_node = lambda o: reverse('catalog-index')
    template_name = 'catalog/print.html'


class OrderView(ToStructureMixin, WithMailer, CreateView):
    """Оформление заявки"""

    model = Order
    form_class = OrderForm
    template_name = 'catalog/order.html'

    def get_template_names(self):
        print self.request.is_ajax()
        if self.request.is_ajax():
            return ['catalog/include/form_ajax.html', ]

        return super(OrderView, self).get_template_names()

    def get_success_url(self):
        if self.request.is_ajax():
            return reverse('catalog-order-success')

        return self.object.item.get_absolute_url() + '#form'

    def form_valid(self, form):
        result = super(OrderView, self).form_valid(form)

        self.send_mail(
            'Заявка на сайте {site_name}',
            'catalog/order/order-mail.html',
            form.cleaned_data,
            XendorSettings().get(u'E-mail администратора'))

        return result


# Объекты по типу
class ObjectsByTypeView(VisibleObjectListMixin, ListByObjectSlugMixin, PaginatedListMixin):
    """все объекты по типу недвижки"""

    template_name = 'catalog/type.html'
    model = Item
    slugified_model = Type
    paginate_by = 12


class SaleByTypeView(ObjectsByTypeView):
    """продажа по типу недвижки"""

    template_name = 'catalog/type-sale.html'

    def get_queryset(self):
        return super(SaleByTypeView, self).get_queryset().filter(deal__in=(u'sale', u'both'))


class RentByTypeView(IsRentMixin, ObjectsByTypeView):
    """аренда по типу недвижки"""

    template_name = 'catalog/type-rent.html'

    def get_queryset(self):
        return super(RentByTypeView, self).get_queryset().filter(deal__in=(u'rent', u'both'))


# Объекты по городу
class ObjectsByCityView(VisibleObjectListMixin, PaginatedListMixin):
    """все объекты по муниципалитету"""

    template_name = 'catalog/city-all.html'
    model = Item
    paginate_by = 12

    def get_queryset(self):
        self.city = get_object_or_404(City, slug=self.kwargs['slug'])
        return super(ObjectsByCityView, self).get_queryset().filter(region__in=self.city.region_set.filter(visible=True))

    def get_context_data(self, **kwargs):
        context = super(ObjectsByCityView, self).get_context_data(**kwargs)

        context.update({'city': self.city})
        return context


class SaleByCityView(ObjectsByCityView):
    """продажа по муниципалитету"""

    template_name = 'catalog/city-sale.html'

    def get_queryset(self):
        return super(SaleByCityView, self).get_queryset().filter(deal__in=(u'sale', u'both'))


class RentByCityView(ObjectsByCityView):
    """аренда по городу"""

    template_name = 'catalog/city-rent.html'

    def get_queryset(self):
        return super(RentByCityView, self).get_queryset().filter(deal__in=(u'rent', u'both'))


class CityView(ToStructureMixin, DetailView):
    """Город"""

    template_name = 'catalog/city.html'
    model = City
    activated_node = lambda o: reverse('catalog-index')
    meta_title = meta_description = breadcrumbs = lambda o: o.get_object().name


# Объекты по району города
class RegionView(ToStructureMixin, DetailView):
    """Регион города"""

    template_name = 'catalog/region.html'
    model = Region
    # activated_node = lambda o: reverse('library-cities')
    meta_title = meta_description = breadcrumbs = lambda o: o.get_object().name


class ObjectsByRegionView(VisibleObjectListMixin, ListByObjectSlugMixin, PaginatedListMixin):
    """все объекты по городу"""

    template_name = 'catalog/region-all.html'
    model = Item
    slugified_model = Region
    paginate_by = 12


class SaleByRegionView(ObjectsByRegionView):
    """продажа по региону города"""

    template_name = 'catalog/region-sale.html'

    def get_queryset(self):
        return super(SaleByRegionView, self).get_queryset().filter(deal__in=(u'sale', u'both'))


class RentByRegionView(ObjectsByRegionView):
    """аренда по региону города"""

    template_name = 'catalog/region-rent.html'

    def get_queryset(self):
        return super(RentByRegionView, self).get_queryset().filter(deal__in=(u'rent', u'both'))


class SearchView(ToStructureMixin, SearchByModelMixin, PaginatedListMixin):
    """Поиск объектов по названию"""

    model = Item
    search_fields = ['title', 'codename']
    paginate_by = 12

    template_name = 'catalog/search.html'

    activated_node = lambda o: reverse('catalog-index')
    meta_title = meta_description = breadcrumbs = u'Поиск'

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context.update({
            'regions': [int(item) for item in self.request.GET.getlist('region')],
            'typies': [int(item) for item in self.request.GET.getlist('type')],
            'rooms': [item for item in self.request.GET.getlist('sleeping_room')],
            'deal_type': self.request.GET.getlist('deal_type'),
        })
        return context

    def get_queryset(self):
        search_fields = self.request.GET
        queryset = super(SearchByModelMixin, self).get_queryset().filter(visible=True)

        if self.request.GET.get(self.search_get_parameter):
            queryset = queryset.filter(reduce(lambda f, s: f | s,
                                              [Q(**{
                                              (f + '__icontains'): self.request.GET.get(self.search_get_parameter)}) for
                                               f in self.search_fields]))

        if search_fields.get('type'):
            queryset = queryset.filter(type__in=search_fields.getlist('type'))

        if search_fields.get('sleeping_room'):
            queryset = queryset.filter(sleeping_room__in=search_fields.getlist('sleeping_room'))

        if search_fields.get('region'):
            queryset = queryset.filter(region__in=search_fields.getlist('region'))

        if search_fields.get('price_low'):
            try:
                price = float(search_fields.get('price_low'))
                queryset = queryset.filter(
                    Q(price_sale__gte=price) | Q(price_sale_discount__gte=price) | Q(price_rent__gte=price) | Q(
                        price_rent_discount__gte=price))
            except:
                pass

        if search_fields.get('price_high'):
            try:
                price = float(search_fields.get('price_high'))
                queryset = queryset.filter(
                    Q(price_sale__lte=price, price_sale__gt=0) | Q(price_sale_discount__lte=price,
                                                                   price_sale_discount__gt=0) | Q(price_rent__lte=price,
                                                                                                  price_rent__gt=0) | Q(
                        price_rent_discount__lte=price, price_rent_discount__gt=0))
            except:
                pass

        if search_fields.get('deal_type'):
            deal_type = search_fields.getlist('deal_type')
            deal_type.append('both')
            queryset = queryset.filter(deal__in=deal_type)

        return queryset


class PostObjectForm(forms.Form):

    description = forms.CharField(widget=forms.Textarea(), label=_(u'Описание'), required=False)
    deal = forms.ChoiceField(
        widget=forms.RadioSelect(),
        choices=[(d[0], d[1]) for d in Item.DEAL_CHOICES if d[0] != u'both'],
        label=_(u'Тип сделки'))
    price = forms.DecimalField(label=_(u'Цена в евро'), decimal_places=2)
    square_live = forms.DecimalField(label=_(u'Жилая площадь'), required=False)
    square_all = forms.DecimalField(label=_(u'Общая площадь'), required=False)
    square = forms.DecimalField(label=_(u'Площадь земли'), required=False)
    region = forms.ChoiceField(choices=City.get_all_regions(), label=_(u'Город и регион'))
    type = forms.ChoiceField(
        choices=[('', _(u'-- Выберите тип недвижимости'))] + list(Type.objects.filter(visible=True).values_list('pk', 'name')),
        label=_(u'Тип недвижимости'))
    title = forms.CharField(label=_(u'Название'), required=False)
    sleeping_room = forms.ChoiceField(
        choices=Item.SLEEPING_ROOM,
        widget=forms.RadioSelect(),
        label=_(u'Количество спален'))
    bath_room = forms.IntegerField(label=_(u'Количество ванн'), min_value=0, required=False)

    person = forms.CharField(label=_(u'Представьтесь'), required=False)
    mail = forms.CharField(label=_(u'E-mail'), required=False)
    phone = forms.CharField(label=_(u'Телефон'), required=False)
    captcha = CaptchaField(label=_(u'Проверочный код '))

    def __init__(self, *args, **kwargs):
        super(PostObjectForm, self).__init__(*args, **kwargs)

        categories = TagCategory.objects.all()
        for category in categories:
            for tag in category.tag_set.all():
                self.fields['tag_' + str(tag.pk)] = forms.CharField(
                    widget=forms.TextInput(),
                    required=False
                )

    def clean_region(self):
        region = self.cleaned_data['region']
        if region == '0':
            return None
        else:
            return Region.objects.get(pk=region)

    def clean_type(self):
        obj_type = self.cleaned_data['type']
        if obj_type == '0':
            return None
        else:
            return Type.objects.get(pk=obj_type)

    def save(self):
        # print self.cleaned_data
        with auto_populate(True):
            obj = Item(
                region=self.cleaned_data['region'],
                type=self.cleaned_data['type'],
                title=self.cleaned_data['title'],
                description=self.cleaned_data['description'],
                square_live=self.cleaned_data['square_live'] or 0,
                square_all=self.cleaned_data['square_all'] or 0,
                square=self.cleaned_data['square'] or 0,
                sleeping_room=self.cleaned_data['sleeping_room'],
                bath_room=self.cleaned_data['bath_room'] or 0,
                deal=self.cleaned_data['deal'],
                codename=unicode(int(time.time())),
                visible=False,

                #данные владельца
                person=self.cleaned_data['person'],
                mail=self.cleaned_data['mail'],
                phone=self.cleaned_data['phone'],
            )

            if self.cleaned_data['deal'] == u'sale':
                obj.price_sale = self.cleaned_data['price']

            if self.cleaned_data['deal'] == u'rent':
                obj.price_rent = self.cleaned_data['price']

            obj.save()

            for k, v in self.cleaned_data.items():
                if k.split('_')[0] == 'tag':
                    tag = Tag.objects.get(pk=k.split('_')[1])
                    if v:
                        property = ObjectProperty(
                            item=obj,
                            tag=tag,
                            value=v
                        )
                        property.save()

        return obj


class PostObjectView(FormView):
    """Добавить объект"""

    template_name = 'catalog/post-object.html'
    app_extension = 'post-object'
    form_class = PostObjectForm

    def get_success_url(self):
        return reverse('catalog-post-object-images')

    def get_context_data(self, **kwargs):
        context = super(PostObjectView, self).get_context_data(**kwargs)

        context.update({
            'tag_categories': TagCategory.objects.all(),
            'tag_values': TagValue.objects.all(),
        })

        return context

    def form_valid(self, form):
        obj = form.save()
        self.request.session['processed_object'] = obj.pk
        return super(PostObjectView, self).form_valid(form)


class PostObjectImagesView(WithMailer, FormView):
    """Добавление картинок в объект"""

    template_name = 'catalog/post-object-image.html'
    form_class = forms.Form
    success_url = '/ru/catalog/post-object/'
    app_extension = 'post-object'

    def get_context_data(self, **kwargs):
        context = super(PostObjectImagesView, self).get_context_data(**kwargs)

        obj = get_object_or_404(Item, pk=self.request.session.get('processed_object'))

        return context

    def form_valid(self, form):
        if self.request.session.get('processed_object'):
            obj = get_object_or_404(Item, pk=self.request.session.get('processed_object'))
            #del self.request.session['processed_object']
            self.send_mail(
                'Добавление объекта на {site_name}',
                'catalog/order/post-ad-mail.html',
                {'obj_id': obj.pk},
                XendorSettings().get(u'E-mail администратора'))

        return super(PostObjectImagesView, self).form_valid(form)


from ajaximage.forms import FileForm


class ProcessImage4PostObject(FormView):
    form_class = FileForm

    def form_valid(self, form):
        import os
        from django.conf import settings
        from django.core.files.storage import default_storage
        from django.http import HttpResponse
        from django.utils.text import slugify

        FILENAME_NORMALIZER = getattr(settings, 'AJAXIMAGE_FILENAME_NORMALIZER', slugify)

        file_ = form.cleaned_data['file']

        file_name, extension = os.path.splitext(file_.name)
        safe_name = '{0}{1}'.format(FILENAME_NORMALIZER(file_name), extension)

        name = os.path.join('catalog/itemimage', safe_name)
        path = default_storage.save(name, file_)

        obj = self.request.session.get('processed_object')
        obj = Item.objects.get(pk=obj)
        with auto_populate(True):
            img = ItemImage(
                image=path,
                item=obj,
                title=self.request.FILES['file'].name
            )
            img.save()

        return HttpResponse('ok')


class XMLExportView(TemplateView):
    """Экспорт объектов в xml"""

    template_name = 'catalog/xml-export.html'

    def get_context_data(self, **kwargs):
        context = super(XMLExportView, self).get_context_data(**kwargs)

        context.update({
            'types': Type.objects.filter(visible=True),
            'items': Item.objects.filter(visible=True),
            'now': int(time.time())
        })
        return context