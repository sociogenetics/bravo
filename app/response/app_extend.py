# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='response',
    app_name=u'Отзывы',
    node_url=lambda: reverse('response-index'),
    children=[],
    node_parameters={},
    safe_for_structure=False
)

