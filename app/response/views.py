# -*- coding: utf-8 -*-
from xendor.cbv import PaginatedListMixin, PageAppExtensionMixin, VisibleObjectListMixin

from .models import Response


class Index(PageAppExtensionMixin, PaginatedListMixin, VisibleObjectListMixin):
    """индекс"""

    template_name = 'response/index.html'
    model = Response
    paginate_by = 12
    app_extension = 'response'