# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField


class Response(models.Model):
    """Отзывы"""

    person = models.CharField(u'Персона', max_length=255)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')
    text = HTMLField(u'Текст')
    visible = models.BooleanField(u'Опубликовано', default=False)
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    def __unicode__(self):
        return self.person + '::' + unicode(self.created)

    class Meta:
        ordering = u'-created',
        verbose_name = u'отыв'
        verbose_name_plural = u'Отзывы'