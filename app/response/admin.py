# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Response


class ResponseAdmin(admin.ModelAdmin):
    list_display = 'person', 'title', 'created', 'visible'


admin.site.register(Response, ResponseAdmin)