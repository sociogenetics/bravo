# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField
from ajaximage.fields import AjaxImageField

from xendor.utils import generate_slug


class Artice(models.Model):
    """Статьи"""

    title = models.CharField(u'Заголовок статьи', max_length=245)
    created = models.DateField(u'Создано', auto_now_add=True)
    anons = models.TextField(u'Анонс')
    text = HTMLField(u'Текст')
    visible = models.BooleanField(u'Опубликовано', default=False)
    in_side = models.BooleanField(u'Отображать в ухе', default=False)

    slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    image = AjaxImageField(u'Главное изображение', upload_to='article', blank=True)


    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.title)
        super(Artice, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('article-item', [self.slug,])

    class Meta:
        ordering = '-created',
        verbose_name = u'статья'
        verbose_name_plural = u'Статьи'


class Photo(models.Model):
    """Изображение"""

    article = models.ForeignKey(Artice, verbose_name=u'статья')
    image = AjaxImageField(u'Изображение', upload_to='articlephoto', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'