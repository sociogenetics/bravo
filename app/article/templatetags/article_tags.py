# -*- coding: utf-8 -*-
from django import template

from ..models import Artice

from xendor.settings import XendorSettings

register = template.Library()


@register.inclusion_tag('article/tags/side.html', takes_context=True)
def article_side(context):
    """Боковое ухо со списком статей"""

    return {
        'articles': Artice.objects.filter(visible=True, in_side=True)[:10]
    }