# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView

from xendor.cbv import ToStructureMixin, PaginatedListMixin, PageAppExtensionMixin, VisibleObjectListMixin

from .models import Artice


class Index(PageAppExtensionMixin, PaginatedListMixin, VisibleObjectListMixin, ListView):
    """индекс"""

    model = Artice
    template_name = 'article/index.html'
    paginate_by = 12
    app_extension = u'article'


class Item(ToStructureMixin, DetailView):
    """статья"""

    model = Artice
    activated_node = lambda o: reverse('article-index')
    template_name = 'article/item.html'
    meta_title = lambda o: o.get_object().meta_title or o.get_object().title
    meta_description = lambda o: o.get_object().meta_description or o.get_object().title
    meta_keywords = lambda o: o.get_object().meta_keywords or o.get_object().title
    breadcrumbs = lambda o: o.get_object().title