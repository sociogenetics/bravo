# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from .models import Artice, Photo


class ArticleTranslationOptions(TranslationOptions):
    fields = ('title', 'anons', 'text', 'meta_title', 'meta_description', 'meta_keywords')


class PhotoTranslationOptions(TranslationOptions):
    fields = ('title', )


translator.register(Artice, ArticleTranslationOptions)
translator.register(Photo, PhotoTranslationOptions)
