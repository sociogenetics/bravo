# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ajaximage.fields
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Artice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=245, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0430\u0442\u044c\u0438')),
                ('created', models.DateField(auto_now_add=True, verbose_name='\u0421\u043e\u0437\u0434\u0430\u043d\u043e')),
                ('anons', models.TextField(verbose_name='\u0410\u043d\u043e\u043d\u0441')),
                ('text', tinymce.models.HTMLField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('visible', models.BooleanField(default=False, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d\u043e')),
                ('in_side', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u0432 \u0443\u0445\u0435')),
                ('slug', models.SlugField(null=True, max_length=250, blank=True, unique=True, verbose_name='\u0421\u043b\u0430\u0433')),
                ('image', ajaximage.fields.AjaxImageField(verbose_name='\u0413\u043b\u0430\u0432\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('meta_title', models.CharField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 title', max_length=255, verbose_name='\u041c\u0435\u0442\u0430 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('meta_description', models.TextField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 description', verbose_name='\u041c\u0435\u0442\u0430 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('meta_keywords', models.TextField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 keywords', verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 ', blank=True)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': '\u0441\u0442\u0430\u0442\u044c\u044f',
                'verbose_name_plural': '\u0421\u0442\u0430\u0442\u044c\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', ajaximage.fields.AjaxImageField(verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('title', models.CharField(default=b'', max_length=245, null=True, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True)),
                ('article', models.ForeignKey(verbose_name='\u0441\u0442\u0430\u0442\u044c\u044f', to='article.Artice')),
            ],
            options={
                'ordering': ('pk',),
                'verbose_name': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
    ]
