# -*- coding: utf-8 -*-
from django import forms
from django.db.models.query import QuerySet
from django.utils.translation import ugettext_lazy as _

from .models import Field, FormSaving

from captcha.fields import CaptchaField


class FeedbackFormPrototype(forms.Form):
    """Прототип формы формгенератора,
        в зависимости от переданной сигнатуры формирует инстанс объекта Form с соответсвующим набором полей"""

    # captcha = CaptchaField(label=_(u'Проверочный код '))

    def __init__(self, field_signature, *args, **kwargs):
        super(FeedbackFormPrototype, self).__init__(*args, **kwargs)
        
        self.field_signature = field_signature

        for field in self.field_signature:
            if field['type'] == 'text':
                self.fields[field['id']] = forms.CharField(
                    label=_(field['label']),
                    widget=forms.TextInput(attrs={'placeholder': field.get('placeholder', '')}),
                    required=field.get('required', False),
                    initial=field.get('initial') or field.get('value'),
                    help_text=field.get('help_text')
                )

            if field['type'] == 'textarea':
                self.fields[field['id']] = forms.CharField(
                    label=_(field['label']),
                    widget=forms.Textarea(attrs={'rows':'3', 'placeholder': field.get('placeholder', '')}),
                    required=field.get('required', False),
                    initial=field.get('initial') or field.get('value'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'boolean':
                self.fields[field['id']] = forms.CharField(
                    label=_(field['label']),
                    widget=forms.CheckboxInput(),
                    required=field.get('required', False),
                    initial= field.get('initial') or field.get('value'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'email':
                self.fields[field['id']] = forms.EmailField(
                    label=_(field['label']),
                    widget=forms.TextInput(attrs={'placeholder': field.get('placeholder', '')}),
                    required=field.get('required', False),
                    initial=field.get('initial') or field.get('value'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'choices':
                self.fields[field['id']] = forms.ChoiceField(
                    label=_(field['label']),
                    required=field.get('required', False),
                    choices=field.get('value', []),
                    initial=field.get('initial'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'multichoices':
                self.fields[field['id']] = forms.MultipleChoiceField(
                    label=_(field['label']),
                    required=field.get('required', False),
                    choices=field.get('value', []),
                    initial=field.get('initial'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'multichoicescheckbox':
                self.fields[field['id']] = forms.MultipleChoiceField(
                    label=_(field['label']),
                    widget=forms.CheckboxSelectMultiple(),
                    required=field.get('required', False),
                    choices=field.get('value', []),
                    initial=field.get('initial'),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'radio':
                self.fields[field['id']] = forms.ChoiceField(
                    label=_(field['label']),
                    widget=forms.RadioSelect(),
                    required=field.get('required', False),
                    choices=field.get('value', []),
                    initial=field.get('initial', None),
                    help_text=field.get('help_text')
                )
            if field['type'] == 'hidden':

                self.fields[field['id']] = forms.CharField(
                    label=_(field['label']),
                    widget=forms.HiddenInput(),
                    required=field.get('required', False),
                    initial=field.get('initial') or field.get('value'),
                    help_text=field.get('help_text')
                )
            self.fields[field['id']].field_type = field['type']

        self.fields['captcha'] = CaptchaField(label=_(u'Проверочный код '))


def _render_field_value(field_type, value):
    """Рендер возможных значений для поля"""

    if field_type in ('radio', 'multichoicescheckbox', 'multichoices', 'choices'):
        return [(lambda l: (lambda i: (len(i) > 1 and i) or False)(l.split('->')) or (l,l))(line)
                for line in value.splitlines()]
    else:
        return value


def _render_field_initial(field_type, value):
    """Рендер значений по умолчаню для поля"""

    if field_type in ('radio', 'multichoicescheckbox', 'multichoices', 'choices'):
        return [unicode(x).strip() for x in value.split(',')]
    else:
        return unicode(value).strip()


def _render_saved_field(field_type, value):
    """Рендер сохраняемых значений полей"""

    if field_type in ('radio', 'multichoicescheckbox', 'multichoices'):
        return "\n".join(value)
    else:
        return value


class FeedbackForm(object):
    """Генератор и процессор для формы"""

    def __init__(self):
        """Конструктор формирует формгенератора по списку полей в базе"""

        self.signature = []
        self._collect_signature()

    def get_caption(self):
        return u'Форма обратной связи'

    def get_form(self):
        """инициализация и получение формы"""

        try:
            return self.form
        except AttributeError:
            self.form = FeedbackFormPrototype(self.signature)
            return self.form

    def _collect_signature(self):
        """Собираем все поля и формируем сигнатуру для формы"""

        self.fields = []
        for field in Field.objects.all():
            self.fields.append(field)
            self.signature.append(
                {
                    'id': 'formfield_' + str(field.pk),
                    'label': field.label,
                    'type': field.field_type,
                    'required': field.required,
                    'placeholder': field.placeholder,
                    'help_text': field.help,
                    'value': _render_field_value(field.field_type, field.value),
                    'initial': _render_field_initial(field.field_type, field.initial),
                }
            )

    def set_default(self, name, value):
        """Интерфейс для изменения какого-либо значения поля"""

        try:
            field = [s for s in self.signature if s['id'] == name][0]
            index = self.signature.index(field)
            field['initial'] = (lambda t, v, fv:
                                t not in ('radio', 'multichoicescheckbox', 'multichoices', 'choices')
                                and _render_field_initial(t, v)
                                or [f[0] for f in fv if f[0] in v or f[1] in v]
                )(field['type'], value, field['value'])
            self.signature[index] = field
        except KeyError:
            pass

    def set_defaults(self, values = []):
        """Интерфейс для инициализации формы данными 
        на входе массив вида [(field_id, field_value), (field_id, field_value), ]
        """

        for item in values:
            self.set_default(item[0], item[1])

    def set_value(self, name, value):
        """Установить возможный список значений для поля
        возможные значения:
            строка (инициализация по типу значений задаваемых в админке)
            список ((value, label),(value, label), ...) - для селектов и мультиселектов
            QuerySet объект (в качестве значений - айдишники, в качестве меток -__unicode__() метод объекта
        """

        try:
            field = [s for s in self.signature if s['id'] == name][0]
            index = self.signature.index(field)

            if isinstance(value, (list, tuple)):
                field['value'] = value

            if isinstance(value, (str, unicode)):
                field['value'] = _render_field_value(field['type'], value)

            if isinstance(value, (QuerySet,)):
                field['value'] = [(i.pk, unicode(i),) for i in value]

            self.signature[index] = field
        except KeyError:
            pass

    def find_field_by_id(self, id):
        """Поиск поля в сигнатуре по его id"""
        return [field for field in self.fields if field.pk == id or str(field.pk) == str(id).replace('formfield_', '')][0]

    def save(self):
        """Сохранение формы"""

        try:
            if self.form.is_valid():
                self._save_by_form()
        except AttributeError:
            self._save_by_initial()

    def _save_by_initial(self):
        """Сохранение по инициализационным данным"""

        saving = FormSaving()
        value = ''
        for f in self.signature:
               value += self.find_field_by_id(f['id']) + ' >> ' +  _render_saved_field(f['type'], f['initial']) + '\n\n\n'

        self.saved_form = saving
        saving.value = value
        saving.save()

    def _save_by_form(self):
        """Сохранение данных из формы"""

        saving = FormSaving()

        value = ''
        for f in self.fields:
            value += unicode(f) + ' : ' + _render_saved_field(f.field_type, self.form.cleaned_data['formfield_' + str(f.pk)]) + '\n\n\n'

        self.saved_form = saving
        saving.value = value
        saving.save()

    def process(self, post_data, **kwargs):
        """Форм-процессинг"""

        default = {
            'save': True,
            'user': None,
            'from_url': None,
        }

        default.update(kwargs)

        self.form = FeedbackFormPrototype(self.signature, data=post_data)
        if self.form.is_valid():
            result = [(f['label'], f['id'], self.form.cleaned_data[f['id']]) for f in self.signature]
            if default['save']:
                self._save_by_form()
                self.saved_form.user = default['user']
                self.saved_form.from_url = default['from_url']
                self.saved_form.save()
            return result
        return False