# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from django.utils.translation import ugettext as _

from xendor.cbv import WithMailer
from xendor.models import Page
from xendor.settings import XendorSettings

from .models import Field
from . import FeedbackForm


XendorSettings().add_settings(u'Адрес офиса', u'г. Екатеринбург, Проспект Ленина, д. 66')
XendorSettings.regenerate()


class Index(WithMailer, FormView):
    """Главная (index)"""

    model = Field
    form_class = None
    template_name = 'contacts/index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        try:
            page = Page.objects.get(app_extension='contacts')
            context.update({'page': page})
        except Page.DoesNotExist:
            pass

        return context

    def get_success_url(self):

        return reverse('contacts-index') + u'#form'

    def get_form(self, form_class):

        self.form_processor = FeedbackForm()
        if self.request.method in ('POST', 'PUT'):
            self.form_processor.process(post_data=self.get_form_kwargs().get('data'))

        return self.form_processor.get_form()

    def form_valid(self, form):
        self.send_mail(
            'Форма обратной связи на сайте {site_name}',
            'contacts/mail.html',
            {'value': self.form_processor.saved_form.value}, XendorSettings().get(u'E-mail администратора'))

        from django.contrib import messages
        messages.add_message(self.request, messages.SUCCESS, _(u'Спасибо! Заявка отправлена администрации сайта.'))
        return super(Index, self).form_valid(form)



