# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Field, FormSaving


class FieldAdmin(admin.ModelAdmin):
    admin_label = u'Модуль контакты'
    admin_doc = u''' 
      Вы можете редактировать поля для ввода в форме контактов
    '''
    
    list_display = ('label', 'required', 'order',)
    list_editable = ('required', 'order',)

admin.site.register(Field, FieldAdmin)

class FormSavingAdmin(admin.ModelAdmin):
    admin_label = u'Модуль контакты'
    admin_doc = u''' 
      Здесь хранятся данные с сохраненных форм
    '''
    
    list_display = '__unicode__',
        
admin.site.register(FormSaving, FormSavingAdmin)
