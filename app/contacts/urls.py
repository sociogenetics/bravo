# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from .views import Index

urlpatterns = patterns('',
    #страница контактов
    url(r'^$', Index.as_view(), name = 'contacts-index'),
)