# -*- coding: utf-8 -*-
from django import template

register = template.Library()

@register.inclusion_tag('contacts/tags/map.html', takes_context=True)
def contacts_map(context):
    """яндекс карта"""

    return context