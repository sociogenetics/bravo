# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

APP_LABEL_FOR_ADMIN_MENU = u'Заказать звонок'

#TODO: сделать тип поля на внешних ключах, для этого разработать формат указания контент тайпа и условий выборки
FIELD_TYPES = (
    ('text', 'Текстовое поле'),
    ('textarea', 'Текстариа'),
    ('email', 'Email'),
    ('boolean', 'Чекбокс'),
    ('choices', 'Селектбокс'),
    ('multichoices', 'Мультиселектбокс'),
    ('multichoicescheckbox', 'Мультиселектбокс чекбоксами'),
    ('radio', 'Радиогруппа'),
    ('hidden', 'Скрытое поле'),
)


class Field(models.Model):
    """Поля для формы"""

    label = models.CharField('Метка поля', max_length=255)

    field_type = models.CharField('Тип поля', max_length=255, choices=FIELD_TYPES)

    required = models.BooleanField('Обязательное поле', default=False)

    help = models.CharField('Пояснительный текст', max_length=255, blank=True)

    value = models.TextField('Возможные значения', blank=True,
        help_text='Не используется для полей типа текстовое поле, текстариа, чекбокс')

    initial = models.CharField('Значения по умолчанию', max_length=255, blank=True,
        help_text='Если тип поля допускает множественный выбор, перечислите значения через запятую')

    placeholder = models.CharField('Подсказка при заполнении', max_length=255, blank=True)

    order = models.IntegerField('Порядок вывода', default=350)

    def __unicode__(self):
        return self.label

    class Meta:
        ordering = 'order',
        verbose_name = 'поле для формы'
        verbose_name_plural = 'Набор полей формы'


class FormSaving(models.Model):
    """Контейнер для сохраненных полей формы, фиксирует сохраненную форму"""

    created = models.DateTimeField('Время сохранения', auto_now_add=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True, help_text='Если пользователь не указан, значит это анонимный пользователь')
    value = models.TextField('Данные формы')

    def get_data(self):
        return self.value

    def __unicode__(self):
        return u'Данные формы от ' + str(self.created)

    class Meta:
        verbose_name = 'сохраненные данные'
        verbose_name_plural = 'сохраненные данные'