# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='contacts',
    app_name=u'Контакты',
    node_url=lambda: reverse('contacts-index'),
    children=[],
    node_parameters={},
    safe_for_structure=True
)