# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

from xendor.structure import Structure

Structure().register_app(
    app_id='news',
    app_name=u'Новости',
    node_url=lambda: reverse('news-index'),
    children=[],
    node_parameters={},
    safe_for_structure=True
)



