# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView

from xendor.cbv import ToStructureMixin, PaginatedListMixin, PageAppExtensionMixin, VisibleObjectListMixin

from .models import News


class Index(PageAppExtensionMixin, PaginatedListMixin, VisibleObjectListMixin, ListView):
    """индекс"""

    model = News
    template_name = 'news/index.html'
    paginate_by = 12
    app_extension = u'news'


class Item(ToStructureMixin, DetailView):
    """Новость"""

    model = News
    activated_node = lambda o: reverse('news-index')
    template_name = 'news/item.html'
    meta_title = meta_description = breadcrumbs = lambda o: o.get_object().title