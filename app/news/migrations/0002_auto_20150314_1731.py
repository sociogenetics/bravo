# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='meta_description',
            field=models.TextField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 description', verbose_name='\u041c\u0435\u0442\u0430 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='news',
            name='meta_keywords',
            field=models.TextField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 keywords', verbose_name='\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 ', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='news',
            name='meta_title',
            field=models.CharField(help_text='\u041c\u0435\u0442\u0430\u0442\u0435\u0433 title', max_length=255, verbose_name='\u041c\u0435\u0442\u0430 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
            preserve_default=True,
        ),
    ]
