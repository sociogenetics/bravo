# -*- coding: utf-8 -*-
from django.db import models

from tinymce.models import HTMLField
from ajaximage.fields import AjaxImageField

from xendor.utils import getmd5
from xendor.utils import generate_slug

         
class Response(models.Model):
    title = models.CharField(u'Имя', max_length=245)
    anons = models.TextField(u'Отзыв')
    visible = models.BooleanField(u'Опубликовано', default=False)
    
    order = models.IntegerField(u'Порядок', help_text = u'Чем меньше число тем выше приоритет вывода пунктов', default=350, blank=True, null=True, editable = False)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = 'order',
        verbose_name = u'отзыв'
        verbose_name_plural = u'Отзывы'


class News(models.Model):
    """Новость"""

    title = models.CharField(u'Заголовок новости', max_length=245)
    created = models.DateField(u'Дата публикации', null=True, blank=True)
    anons = models.TextField(u'Анонс')
    text = HTMLField(u'Текст новости')
    visible = models.BooleanField(u'Опубликовано', default=False)

    slug = models.SlugField(u'Слаг', max_length=250, null=True, unique=True, blank=True)

    image = AjaxImageField(u'Главное изображение', upload_to='news', blank=True)

    # Метатеги
    meta_title = models.CharField(u'Мета заголовок', max_length=255, help_text=u'Метатег title', blank=True)
    meta_description = models.TextField(u'Мета описание', help_text=u'Метатег description', blank=True)
    meta_keywords = models.TextField(u'Ключевые слова ', help_text=u'Метатег keywords', blank=True)

    def save(self, *args, **kwargs):
        self.slug = generate_slug(self, self.slug or self.title)
        super(News, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('news-item', [self.slug,])

    class Meta:
        ordering = '-created',
        verbose_name = u'новость'
        verbose_name_plural = u'Новости'


class Photo(models.Model):
    """Изображение"""

    news = models.ForeignKey(News, verbose_name=u'Новость')
    image = AjaxImageField(u'Изображение', upload_to='newsphoto', blank=True)
    title = models.CharField(u'Заголовок изображения', max_length=245, null=True, blank=True, default='')

    def __unicode__(self):
        return ' :: ' + unicode(self.pk)

    class Meta:
        ordering = 'pk',
        verbose_name = u'изображение'
        verbose_name_plural = u'Изображения'