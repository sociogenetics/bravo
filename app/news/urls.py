# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from .views import Index, Item

urlpatterns = patterns('',
    #Индекс
    url(r'^$', Index.as_view(), name = 'news-index'),
    url(r'^(?P<slug>[\w-]+)/$', Item.as_view(), name = 'news-item'),
)