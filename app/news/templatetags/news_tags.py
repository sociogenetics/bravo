# -*- coding: utf-8 -*-
from django import template

from ..models import News

from app.response.models import Response

from xendor.settings import XendorSettings

register = template.Library()

@register.assignment_tag
def news_last():
    return News.objects.filter(visible=True)[:2]


@register.assignment_tag
def news_response():
    return Response.objects.filter(visible=True).order_by('?')[:4]

@register.assignment_tag
def news_response2():
    return Response.objects.filter(visible=True).order_by('?')[:2]


