# -*- coding: utf-8 -*-
from django.contrib import admin

from grappelli_modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from xendor.admin_utils import image_field

from .models import News, Photo, Response


class ImageInline(TranslationTabularInline):
    model = Photo
    extra = 0


class NewsAdmin(TranslationAdmin):
    date_hierarchy = 'created'
    search_fields = ('title',)
    list_display = ('poster', 'title', 'created', 'visible')
    list_filter = ('visible',)
    list_display_links = ('title', 'created',)
    list_editable = ('visible',)

    admin_label = u'Новости'

    poster = image_field('image', u'Изображение', u'100;200')

    inlines = [
        ImageInline
    ]

admin.site.register(News, NewsAdmin)


# class ResponseAdmin(admin.ModelAdmin):
#     search_fields = ('title',)
#     list_display = ('poster', 'title', 'visible')
#     list_filter = ('visible',)
#     list_display_links = ('title', 'poster')
#     list_editable = ('visible', )
#
#     poster = image_field('image', u'Изображение', u'80;160;fix')
#
# admin.site.register(Response, ResponseAdmin)