# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from .models import News, Photo


class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'anons', 'text', 'meta_title', 'meta_description', 'meta_keywords')


class PhotoTranslationOptions(TranslationOptions):
    fields = ('title', )


translator.register(News, NewsTranslationOptions)
translator.register(Photo, PhotoTranslationOptions)
