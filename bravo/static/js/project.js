/*js for project*/
$(document).ready(function(){
	$("a.fancybox").fancybox({
        helpers : {
    		title : {
    			type : 'over'
    		}
    	}
    });

	$('.ajax-form').ajaxForm(function(data, code, rt) {
		$('form.ajax-form').html(data);
	});

    $(".multiselect").each(function(){
        var header = $(this).attr('htitle')
        $(this).multiSelect({
            selectableOptgroup: true,
            selectableHeader: "<div class='multiselect-header-top'><span class='glyphicon glyphicon-th-list' aria-hidden='true'></span> " + header + "</div>",
            selectionHeader: "<div class='multiselect-header'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span> Выбранные значения</div>"
        });
    });

    var price_low = $('#price_low').val()
    var price_high = $('#price_high').val()

    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 5000000,
        values: [ price_low, price_high ],
        step: 50,
        slide: function( event, ui ) {
            $('#price_low').val(ui.values[ 0 ])
            $('#price_high').val(ui.values[ 1 ])
            $( "#amount" ).val( "€" + ui.values[ 0 ].toString().replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ') + " - €" + ui.values[ 1 ].toString().replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ') );
        }
    });
    $( "#amount" ).val( "€" + $( "#slider-range" ).slider( "values", 0 ).toString().replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ') +
      " - €" + $( "#slider-range" ).slider( "values", 1 ).toString().replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ') );

    
    $('.uppertabs li a').each(function(){
    	if($(this).attr('href') == window.location.pathname){
    		$(this).parent().addClass('active');
    	};
    });

	$('.row.response').each(function(){
		var max = 0;
		$(this).find('.well').each(function(){
			var height = parseInt($(this).height());
			if(height>max){
				max = height;
			}
		});
		$(this).find('.well').css('height', max+40);
	});
	
	return true;
});

