# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin

from filebrowser.sites import site

admin.autodiscover()

urlpatterns = patterns('',
    (r'^tinymce/', include('tinymce.urls')),

    url(r'^ajaximage/', include('ajaximage.urls')),

    url(r'^admin/filebrowser/', include(site.urls)),

    (r'^grappelli/', include('grappelli.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^captcha/', include('captcha.urls')),
)

urlpatterns += i18n_patterns('',

    url(r'^response/', include('app.response.urls')),

    url(r'^contacts/', include('app.contacts.urls')),

    url(r'^article/', include('app.article.urls')),

    url(r'^news/', include('app.news.urls')),

    url(r'^catalog/', include('app.catalog.urls')),

    url(r'^capture/', include('app.capture.urls')),

    # XDP-page
    url(r'', include('xendor.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )